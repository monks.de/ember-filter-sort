**ember-filter-sort** is an [Ember](https://emberjs.com/)-addon for filtering
and sorting arrays of data.

Also check out this little
[example-application](https://gitlab.com/monks.de/ember-filter-sort-example)
using this addon.

# Key Features

* Unlike other Ember-addons with a similar purpose, **ember-filter-sort** is a
  [Mixin](https://www.emberjs.com/api/ember/2.14/classes/Ember.Mixin) based on
  actions. In particular, it's not a
  [component](https://www.emberjs.com/api/ember/2.14/classes/Ember.Component).
  This way it doesn't restrict you to a certain template. Thus, you can use it
  with your own tags in your own template.

* You can filter your data by one property or multiple properties using
  different functions. **ember-filter-sort** ships some functions already but
  you can also define your own. For instance, given array of objects
  representing humans, you could filter those whose names begin with "ann" or
  those whose name contains "ann".

* Similarly, you can sort your data by one property using different functions.
  You can use those functions shipped with **ember-filter-sort** or define
  your own.

# Guide

## Installation

In your project directory, run:

```sh
ember install ember-filter-sort
```

## Mix-in FilsorMixin

First import the `FilsorMixin` module into your component or controller:

```javascript
import FilsorMixin from 'ember-filsor-table/mixins/filsor-mixin';
```


Now you can mix-in the `FilsorMixin` into your component or controller:

```javascript
export default Ember.Controller.extend(FilsorMixin, {
  //...
});
```

## Set-up `filsorDataKey` and use `filsorData`

Let's assume you use the
[`each`](https://emberjs.com/api/ember/2.14/classes/Ember.Templates.helpers/methods/each?anchor=each)
template-helper to loop over your data in your components or controllers
template. For example:

```hbs
<ul>
  {{#each yourData as |datum|}}
    <li>{{datum.firstName}}</li>
    <li>{{datum.lastName }}</li>
  {{/each}}
</ul>
```

Replace the above code with

```hbs
<ul>
  {{#each filsorData as |datum|}}
    <li>{{datum.firstName}}</li>
    <li>{{datum.lastName }}</li>
  {{/each}}
</ul>
```

and don't forget to set the `filsorDataKey` property in your component or
controller:
```javascript
export default Ember.Controller.extend(FilsorMixin, {
  //...
  filsorDataKey: 'yourData',
  //...
});
```

## Take action

Now you can use the actions which the `FilsorMixin` delivers.

### Filter

#### Concrete Example

You can now filter your data by using the `filsorFilter` action. The following
action will filter `yourData` by the `firstName` property. 

```hbs
{{action "filsorFilter" "search" "firstName"}}
```

The `"search"` parameter denotes that the action will check the `firstName`
property of each datum if it contains the value of the
`filsorFilteredFirstName` variable. If you'd rather like to filter by a
prefix, you could use the `prefix` filter function, like this:

```hbs
{{action "filsorFilter" "prefix" "firstName"}}
```

A complete example of a textual input-field for filtering could look like
this:

```hbs
{{input
  key-up=(action "filsorFilter" "search" "firstName")
  value=filsorFilteredFirstName
}}
```

But of course you're free to use which ever markup you want. You could also
use a `<button>`, for example, to fire the action.

#### General

The general syntax is like this:

```hbs
{{action
    "filsorFilter"
    filterFunctionKey
    propertyKey
    [...additionalParametersForFilterFunction]
}}
```

The variable name will then be `filsorFiltered<CamelCasedPropertyKey>`.

##### Out-of-the-box Filter Functions

* `search`: Will case-insensitively check if the property of each datum
  includes each whitespace-separated word in the query.

* `sequence`: E.g., when first time called consecutively, `{{action "filter"
  propertyKey "one" "two" "three"}}` will check if the property of each datum
  equals `"one"`. The second time called it will check if it equals `"two"`,
  etc.

* `prefix`: Will case-insensitively check if the query is a prefix of the
  property of each datum.

* `gteq`: Will check if query is greater than or equal the property of each
  datum.

You could have written those filter functions yourself and you can define your
own filter functions additionally.

### Sort

#### Concrete Example

Sort your data by using the `filsorSort` action. The following action will
sort `yourData` by their `firstName` property. 

```hbs
{{action "filsorSort" "default" "firstName"}}
```

The `"default"` parameter denotes that the action will use the default
comparison, that is, it will sort alphabetically. If you'd rather like to sort
numerically, you could use the `numeric` sort function, like this:

```hbs
{{action "filsorSort" "numeric" "firstName"}}
```

The sort function will know how many times it has already been called
consecutively so far. The default sort functions will use this information to
toggle between ascending and descending order each time their are called
again.

#### General

```hbs
{{action
    "filsorSort"
    sortFunctionKey
    propertyKey
    [...additionalParametersForSortFunction]
}}
```

##### Out-of-the-box Sort Functions

* `default`: Will sort the data ascendingly using Javascript's `<`. The next
  call will sort the data descendingly.

* `sequence`: Allows you to specify the order of the properties explicitely:
  `{{action "sequence" propertyKey "one" "two" "three"}}`. If the property is
  not listed it will dealt as the biggest.

* `numeric`: Tries to interpret the property as floating-point number.

* `reverse`: Will first sort descendingly; another call will sort the data
  ascendingly.

You could have written those sort functions yourself and you can define your
own sort functions additionally.

### Reset

The `filsorReset` action will reset all filters and sorts to their zero.

```hbs
{{action "filsorReset"}}
```

## Customization

### Write your own filter function

#### General

In your component or controller, define an **`Ember.Object`** called
`filsorFilter<NameOfFilterFunction>`. It must have these two methods:

* `filter`

    * *Signature*: `(query, ...additionalParams) -> ((propertyKey, datum) ->
      bool)`

    * *Arguments*:

        * *Argument 1*: Query as string.

        * *Optional other arguments*: Additional filter function arguments.

    * *Returns*: A function with following characteristics: 

        * *Description*: It will be used to check if a datum is filtered out
          or in.

        * *Arguments*:

            * *Argument 1*: Property key as string.

            * *Argument 2*: Whole datum object.

        * *Returns*: A boolean holding the decision if datum is filtered out
          or in.

* `prefilter`

    * *Signature*: `(queryKey, propertyKey, ...additionalParams) -> ()`

    * *Description*: This function will be called once each time when the
      `filsorFilter` action is invoked, before the data is filtered. You could
      for instance use it to count how many times the filter function was
      invoked and save the counter in the query.

    * *Arguments*:

        * *Argument 1*: Query key as string.

        * *Argument 2*: Property key as string.

        * *Optional other arguments*: Additional filter function arguments.

    * *Returns*: Nothing.

### Write your own sort function

In your component or controller, define an **`Ember.Object`** called
`filsorSort<NameOfSortFunction>`. It must have the following properties:

* `compare`

    * *Description*: Determines how to compare two data and their
      corresponding weights using a certain property for sorting with the
      current counter.

    * *Signature*: `(counter, propertyKey, ...params) -> ((datum, weight,
      datum, weight) -> number)`

    * *Arguments*:

        * *Argument 1*: Current counter.

        * *Argument 2*: Property key as string.

        * *Optional other arguments*: Additional sort function arguments.

    * *Returns*: A function with following characteristics:
    
        * *Description* It will be used to compare one datum and its weight
           with another datum an its weight.

        * *Arguments*:

            * *Argument 1*: First datum.

            * *Argument 1*: Weight of first datum.

            * *Argument 2*: Second datum.

            * *Argument 2*: Weight of second datum.

        * *Returns*: A negative number if first datum is "smaller" than
          second. Zero if both data are "equal". A positive number otherwise.

    * *Default value* Toggles between ascending and descending on each
      invokation.

* `weight`

    * *Description*: Calculates the weight of property of a datum.

    * *Signature*: `(datum, propertyKey, ...params) -> weight`

    * *Arguments*:

        * *Argument 1*: Datum.

        * *Argument 2*: Property key as string.

        * *Optional other arguments*: Additional sort function arguments.

    * *Returns*: Weight of property of datum.

    * *Default value*: `(datum, propKey) => filsorGet(datum, propKey)`.
    
* `next`

    * *Description*: Determines how to "increment" the counter.

    * *Signature*: `(counter, ...params) -> counter`

    * *Default value*: `counter => counter + 1`.
    
* `zero`

    * *Description*: Determines the value of the counter when it gets reset.

    * *Signature*: `(...params) -> counter`

    * *Arguments*:

        * *Optional other arguments*: Additional sort function arguments.

    * *Default value*: `=> 0`.


## The `filsor-sort-indicator` helper

tbd
