'use strict';

import { isNone } from '@ember/utils';
import { helper } from '@ember/component/helper';

export function filsorSortIndicator (params) {

  if (isNone(params[0]) || params[0] === 0) {
    return params[1];
  }

  else {
    let divisor       = params.length - 2,
        counterMod    = params[0] % divisor,
        nonNegCounter = counterMod < 0 ? counterMod + divisor : counterMod;

    return params[(nonNegCounter % divisor) + 2];
  }
}

export default helper(filsorSortIndicator);
