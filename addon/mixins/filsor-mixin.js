'use strict';

import Mixin from '@ember/object/mixin';
import { typeOf } from '@ember/utils';
import EmberObject from '@ember/object';
import { isEmpty } from '@ember/utils';
import { isPresent } from '@ember/utils';
import { isNone } from '@ember/utils';
import { A } from '@ember/array';

// because `someObject['nested.property.path']` doesn't work, use
// `filsorGet(someObject, 'nested.property.path')` instead.
export function filsorGet (object, propKey) {
  switch (typeOf(object)) {
    // in case of an ember-object, just use `get` because
    // `emberObject.get('nested.property.path')` works.
    case 'instance':
      return object.get(propKey);
    // in case of a javascript-object, make an ember-object out of it and use
    // `get` afterwards.
    case 'object':
      return EmberObject.create(object).get(propKey);
    default:
      return undefined;
  }
}

// if the given first argument is a string, this function will camelCase that
// string; otherwise the object is just returned.
export function filsorMakeKey (object, firstLetterLowerCase) {
  if (typeof(object) === 'string') {
    var camelCase = function (firstLetterLowerCase) {
      if (firstLetterLowerCase) {
        return function (word) {
          return word.charAt(0).toLowerCase() + word.slice(1);
        };
      }
      else {
        return function (word) {
          return word.charAt(0).toUpperCase() + word.slice(1);
        };
      }
    };
    var words = object.split(/[^a-zA-Z0-9]/);
    return camelCase(firstLetterLowerCase === true)(words[0]) + words
      .slice(1)
      .map(camelCase(false))
      .join('');
  }

  else {
    return object;
  }
}

export default Mixin.create({
  init: function(){
    this.filsorInit();
    this._super(...arguments);
  },

  // at the beginning, there are no active filters
  filsorActiveFilters: A([]),

  // at the beginning, there is no active sort
  filsorActiveSort: EmberObject.create({}),

  // initialize data
  'filsorInit': function () {
    //console.log("filsorMixin.filsorInit");
    this.set(
      'filsorData',
      this.get(this.get('filsorDataKey'))
    );
  },

  // filter algorithm which
  //   * case-insensitively
  //   * checks if property includes each whitespace-separated word in query
  'filsorFilterSearch': EmberObject.create({
    prefilter: function () {
      //console.log("filsorMixin.filsorFilterSearch.prefilter");
      return;
    },
    filter: function (query) {
      //console.log("filsorMixin.filsorFilterSearch.filter");
      var queries = query.toLowerCase().match(/\S+/g) || [];
      return function (propKey, datum) {
        var dat = filsorGet(datum, propKey).toString().toLowerCase();
        for (var i = 0, len = queries.length; i < len; i++) {
          if (!dat.includes(queries[i])) {
            return false;
          }
        }
        return true;
      };
    }
  }),

  // filter algorithm which, given parameters,
  // filters properties matching one of
  // by-click-decreasing tail of these parameters
  'filsorFilterSequence': EmberObject.create({
    prefilter: function (queryKey, propKey, ...params) {
      //console.log("filsorMixin.filsorFilterSequence.prefilter");
      var query = this.get(queryKey);
      this.set(
        queryKey,
        (query ? query + 1 : 1) % params.length
      );
      return;
    },
    filter: function (query, ...params) {
      //console.log("filsorMixin.filsorFilterSequence.filter");
      return function (propKey, datum) {
        var prop = filsorGet(datum, propKey);
        for (var i = 0, len = params.length; i < len; i++) {
          if (params[i] === prop.toString()) {
            return i >= query;
          }
        }
        return false;
      };
    }
  }),

  // filter algorithm which case-insensitively checks
  // if property is prefix of query
  'filsorFilterPrefix': EmberObject.create({
    prefilter: function () {
      //console.log("filsorMixin.filsorFilterPrefix.prefilter");
      return;
    },
    filter: function (query) {
      //console.log("filsorMixin.filsorFilterPrefix.filter");
      var lowerCaseQuery = query.toLowerCase();
      return function (propKey, datum) {
        return filsorGet(datum, propKey)
          .toString()
          .toLowerCase()
          .startsWith(lowerCaseQuery);
      };
    }
  }),

  // filter algorithm which checks
  // if query is greater than or equal the property
  'filsorFilterGteq': EmberObject.create({
    prefilter: function (queryKey) {
      //console.log("filsorMixin.filsorFilterGteq.prefilter");
      var query = this.get(queryKey);
      this.set(
        queryKey + '-valid',
        isEmpty(query) || /^\d+$/.test(query)
      );
      return;
    },
    filter: function (query) {
      //console.log("filsorMixin.filsorFilterGteq.filter");
      if (isEmpty(query)) {
        return function () {
          return true;
        };
      }
      else {
        return function (propKey, datum) {
          return query >= filsorGet(datum, propKey);
        };
      }
    }
  }),

  // default sort algorithm which
  // toggles between {ascending, descending} order
  'filsorSortDefault': EmberObject.create({
    zero: 0,
    next: function (counter) {
      //console.log("filsorMixin.filsorSortDefault.next");
      return counter + 1;
    },
    weight: function (datum, propKey) {
      //console.log("filsorMixin.filsorSortDefault.weight");
      return filsorGet(datum, propKey);
    },
    compare: function (counter) {
      //console.log("filsorMixin.filsorSortDefault.compare");
      var toggle = counter % 2 === 1 ? -1 : 1;

      return function (datum1, weight1, datum2, weight2) {
        if      (weight1 < weight2) { return  toggle; }
        else if (weight1 > weight2) { return -toggle; }
        else                        { return 0;       }
      };
    }
  }),

  // this sort algorithm uses its extra arguments as a specification of the
  // sort order. for example, you could use
  //   {{action 'filsorSort' 'sequence' 'foo' 'green' 'yellow' 'red'}}
  // to specify that order.
  'filsorSortSequence': EmberObject.create({
    weight: function (datum, propKey, ...params) {
      //console.log("filsorMixin.filsorSortSequence.weight");
      var len = params.length;
      var prop;
      for (var i = 0; i < len; i++) {
        prop = filsorGet(datum, propKey);
        if (params[i] === prop.toString()) {
          return i;
        }
      }
      return len;
    }
  }),

  // this sort algorithm first parses the property as float and sorts the
  // items numerically instead of lexicographically.
  'filsorSortNumeric': EmberObject.create({
    weight: function (datum, propKey) {
      // console.log("filsorMixin.filsorSortNumeric.weight");
      return parseFloat(filsorGet(datum, propKey));
    }
  }),

  // sort algorithm which
  // toggles between {descending, ascending} order
  'filsorSortReverse': EmberObject.create({
    compare: function (counter, propKey) {
      //console.log("filsorMixin.filsorSortReverse.compare");
      return this.get('filsorSortDefault.compare')(counter % 2 === 0 ? -1 : 1, propKey);
    }
  }),

  // re-applies filters and sort
  'filsorApply': function () {
    //console.log("filsorMixin.filsorApply");

    // apply filters
    var data = this
      // get data
      .get(this.get('filsorDataKey'))
      // actualy filter
      .filter(
        function (datum) {
          return this
            // get active filters
            .get('filsorActiveFilters')
            // check if every active filter passes
            .every(
              function (filter) {

                // get query
                var query = this.get('filsorFiltered' + filsorMakeKey(filter.propKey));

                if (query == null) {
                  return true;
                }

                return this
                  .get('filsorFilter' + filsorMakeKey(filter.filterKey))
                  .filter
                  .call(
                    this,
                    query,
                    ...filter.params
                  )(
                    filter.propKey,
                    datum
                  );
              },
              this
            );
        },
        this);

    var activeSort = this.get('filsorActiveSort');

    // if there is an active sort, apply active sort
    if (isPresent(activeSort.sortKey)) {

      // get weight function
      var weight = this.get(
        'filsorSort' +
        filsorMakeKey(this.get('filsorActiveSort.sortKey')) +
        '.weight'
      );

      // if weight function isn't given, use default
      if (isNone(weight)) {
        weight = this.get('filsorSortDefault.weight');
      }

      // get compare function
      var compare = this.get(
        'filsorSort' +
        this.get('filsorActiveSort.sortKey') +
        '.compare'
      );

      // if compare function isn't given, use default
      if (isNone(compare)) {
        compare = this.get('filsorSortDefault.compare');
      }

      // get active compare function
      var activeCompare = compare.call(
        this,
        activeSort.counter,
        activeSort.propKey,
        ...activeSort.params
      );

      // apply active sort
      data = data
        // weight data
        .map(datum => ({
          datum: datum,
          weight: weight(datum, activeSort.propKey, ...activeSort.params)
        }))
        // sort data by weight
        .sort(function (weightedDatum1, weightedDatum2) {
          return activeCompare(
            weightedDatum1.datum,
            weightedDatum1.weight,
            weightedDatum2.datum,
            weightedDatum2.weight
          );})
        // remove weight
        .map(weightedDatum => weightedDatum.datum);
    }

    // set filtered and sorted data
    this.set('filsorData', data);

    return;
  },

  actions: {

    // action which filters data using the filter given by filterKey
    // referring to property given by propKey.
    'filsorFilter': function (filterKey, propKey, ...params) {
      //console.log("filsorMixin.actions.filsorFilter");

      // call prefilter
      this
        .get('filsorFilter' + filsorMakeKey(filterKey))
        .get('prefilter')
        .call(
          this,
          'filsorFiltered' + filsorMakeKey(propKey),
          propKey,
          ...params
        );

      // add new filter
      this
        .get('filsorActiveFilters')
        .unshiftObject(EmberObject.create({
          propKey: propKey,
          filterKey: filterKey,
          params: params
        }));

      // remove old filter
      // by only keeping the first filter for each property key
      this
        .set(
          'filsorActiveFilters',
          this
            .get('filsorActiveFilters')
            .uniqBy('propKey')
        );

      // re-apply filters and sort
      this.get('filsorApply').call(this);

      return;
    },

    // action which sorts data using property given by newPropKey
    'filsorSort': function (newSortKey, newPropKey, ...params) {
      //console.log("filsorMixin.actions.filsorSort");

      // merge new and default sort
      var sortNew = EmberObject.create(
        this.get('filsorSort' + filsorMakeKey(newSortKey)),
        this.get('filsorSortDefault')
      );

      var activeSort = this.get('filsorActiveSort');

      // case: sorting by same property as last time
      if ((typeOf(activeSort) === 'instance') &&
          (activeSort.get('propKey') === newPropKey)) {

        // increment counter inside filsorActiveSort
        activeSort.set(
          'counter',
          sortNew.next(
            activeSort.get('counter'),
            ...params
          )
        );

        // set counter inside filsorSorted[ACTIVE PROPNAME]
        this.set(
          'filsorSorted' + filsorMakeKey(activeSort.get('propKey')),
          activeSort.get('counter')
        );
      }

      // case: sorting first time (since ever or since last reset) or by
      // different property than last time
      else {

        // case: there is an active sort (i.e. it's not the first time we're
        // sorting since ever or since last reset)
        if (isPresent(activeSort.get('sortKey'))) {
          // reset old=active filsorSorted* to its zero
          this.set(
            'filsorSorted' + filsorMakeKey(activeSort.get('propKey')),
            EmberObject.create(
              this.get('filsorSort' + activeSort.get('sortKey')),
              this.get('filsorSortDefault')
            ).zero
          );
        }

        activeSort.setProperties({
          propKey: newPropKey,
          counter: sortNew.next(sortNew.zero),
          params: params
        });

        this.set(
          'filsorSorted' +
          // because newPropKey equals activeSort.propKey
          filsorMakeKey(newPropKey),
          activeSort.get('counter')
        );
      }

      // set sortKey of active sort
      this.set(
        'filsorActiveSort.sortKey',
        newSortKey
      );

      // apply filters and sorts
      this.get('filsorApply').call(this);

      return;
    },

    // reset active sort and filters and filtered values
    'filsorReset': function () {
      //console.log("filsorMixin.actions.filsorReset");

      // reset active filsorFiltered* variables
      var activeFilters = this.get('filsorActiveFilters');
      if (isPresent(activeFilters)) {
        activeFilters
          .forEach(
            function (filterItem) {
              this.set(
                'filsorFiltered' + filsorMakeKey(filterItem.get('propKey')),
                undefined
              );
            },
            this
          );
      }

      // reset active filters
      this.set('filsorActiveFilters', A([]));

      var activeSort = this.get('filsorActiveSort');

      // if there is an active sort
      if (isPresent(activeSort)) {

        // get active zero
        var activeZero = this.get('filsorSort' + activeSort.get('sortKey') + '.zero');

        if (activeZero === undefined) {
          activeZero = this.get('filsorSortDefault.zero');
        }

        // reset active filsorSorted* vartiable
        this.set(
          'filsorSorted' + filsorMakeKey(activeSort.get('propKey')),
          activeZero
        );
      }

      // reset active sort
      this.set('filsorActiveSort', EmberObject.create({}));

      // apply reset filters and sorts
      this.get('filsorApply').call(this);

      return;
    }

  }

});
