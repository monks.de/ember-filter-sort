# Changelog

## master
* Renamed `Object` in Mixin to `EmberObject`.

## 0.8.8 - 2018-01-19
* Fixing failing test because of wrong `this._super()` call.

## 0.8.7
* Removed last deprecation warning by no longer using `.on('init')`.

## 0.8.6
* Update to ember(-cli) 2.18
